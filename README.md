# Aplicacion en Ionic 3 ejemplo para curso en Udemy
Link del curso: https://www.udemy.com/aprende-ionic-2-desde-cero
## Clonar proyecto
`$ git clone https://gitlab.com/CarlosGalindo/todolist-android-app.git`
## Instalacion de Ionic
**Primero se debe tener Node.js**

https://nodejs.org/en/download/package-manager

**Instalar Ionic 3**

`$ npm install -g ionic`

## Lanzar aplicacion en explorador 

`$ ionic serve`

## Emulacion

***Se necesita tener Android Studio instalado con y el SDK de Android***

**Agregar la plataforma Android al proyecto**

`$ ionic cordova platform add android`

**Emular en algun dispositivo conectado o en un emulador disponible**

`$ ionic cordova run android`

_Nota: Si hay un dispositivo conectado en modo depuracion, la aplicacion se lanzara  con prioridad al dispositivo que al emulador._

## Solucion de errores

**Error de variable de entorno con Android**

`$ export ANDROID_HOME=/<installation location>/android-sdk-linux`

`$ export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools`

**Error de acceso al proyecto**

`$ sudo chmod -R a+rwx /<app folder>`