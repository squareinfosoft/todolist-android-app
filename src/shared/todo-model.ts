export class TodoModel{

    constructor(
        public description:string,
        public isDone:boolean = false,
        public isImportant:boolean = false
    ){}

    /**
     * Metodo para clonar un elemento todo, sin afectar a los objetos 
     * instanciados, ya que no se usa ninguna propiedad del constructor
     * @param item 
     */
    static clone(item:TodoModel){
        return new TodoModel(item.description, item.isDone, item.isImportant);
    }
}