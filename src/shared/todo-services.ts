import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TodoModel } from './todo-model';
/*
  Generated class for the TodoServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TodoServicesProvider {

  private todos: TodoModel[]; 

  constructor(
    public http:HttpClient,
    ) {
    this.getTodos();
  }

  getTodos(){

    this.todos = [
      new TodoModel("This is an element"),
      new TodoModel("This is an element",false,true),
      new TodoModel("This is an element"),
      new TodoModel("This is an element"),
      new TodoModel("This is an element"),
      new TodoModel("This is an element"),
      new TodoModel("This is an element",true,false),
      new TodoModel("This is an element",false,true),
    ]
  }

    /**
   * Metodo para cambiar el valor del checkbox del item
   * if isDone == true, isDone == false now
   * @param item 
   */
  toogleTodo(item:TodoModel){

      let isDone = !item.isDone;
      const todoIndex = this.todos.indexOf(item); //se obtiene el index del item que llega
      let updateTodo = new TodoModel(item.description, isDone, item.isImportant);//se crea un elemento temporal

      //se inyecta los elementos al nuevo arreglo
      this.todos = [
        ...this.todos.slice(0, todoIndex),//lo anterior del item que llega
        updateTodo,//el item en cuestion se agrega
        ...this.todos.slice(todoIndex+1)//se crea la segunda parte del arreglo despues de la actualizacion
      ]

  }

  /**
   * Metodo para agregar una nueva tarea a la lista de tareas 'todos'
   * @param item 
   */
  addTodo(item:TodoModel){
    //this.todos.push(item);
    this.todos = [...this.todos, item];//se pasa el viejo arreglo con uno nuevo elemento
  }

    /**
   * Metodo para remover un elemento de la lista
   * @param item 
   */
  removeTodo(item:TodoModel){
    const index = this.todos.lastIndexOf(item);
    this.todos = [
      ...this.todos.slice(0,index), //obtiene el arreglo desde 0 hasta el alemento
      ...this.todos.slice(index+1) //obtiene la segunda parte del arreglo, desde uno mas que el index hata el final
    ];
  }

  /**
   * Metodo para editar un elemento de la lista
   * @param item 
   */
  showEditTodo(originalTodo:TodoModel, modifiledTodo:TodoModel){
    const index = this.todos.indexOf(originalTodo);
    this.todos = [
      ...this.todos.slice(0,index),
      modifiledTodo,
      ...this.todos.slice(index+1)
    ]
  }
}
