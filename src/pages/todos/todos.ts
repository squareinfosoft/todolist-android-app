import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform } from 'ionic-angular';
import { TodoModel } from '../../shared/todo-model';
import { AddTaskModalPage } from '../add-task-modal/add-task-modal';
import { TodoServicesProvider } from '../../shared/todo-services';

/*
 * Generated class for the TodosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-todos',
  templateUrl: 'todos.html'
})
export class TodosPage {

  private toogleTodoTimeout = null;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalController: ModalController,
    public todoServices: TodoServicesProvider,
    public platform:Platform
  ) {
  }

  /*
  * Metodo que inicializa toda la page
  */
  ionViewDidLoad() {  }

  /**
   * Metodo que da el estilo de el item, dependiendo de si es importanto o no, o si ya se ha cumplido
   * @param item
   */
  setTodoStyles(item:TodoModel){
    let style = {
       'text-decoration': item.isDone ? 'line-through': 'none',
       'font-weight': item.isImportant ? '600' : 'normal'
    }
    return style;
  }

    /**
   * Metodo para cambiar el valor del checkbox del item
   * @param item 
   */
  toogleTodo(item:TodoModel){
    //evitar que se de doble clic en un elemento y altere la logica de la pagina
    if(clearTimeout(this.toogleTodoTimeout))
      return;

    this.toogleTodoTimeout = setTimeout(()=>{
      this.todoServices.toogleTodo(item);
      this.toogleTodoTimeout = null;
    }, this.platform.is('ios')?0:300)
    

  }

  /**
   * Metodo para mostrar la pagina emergente para agregar una nueva tarea
   */
  showAddTodo(){
    let modal = this.modalController.create(AddTaskModalPage);
    modal.present();

    modal.onDidDismiss(data => {
      if(data)
        this.todoServices.addTodo(data);
    });
  }

  /**
   * Metodo para remover un elemento de la lista
   * @param item 
   */
  removeTodo(item:TodoModel){
    this.todoServices.removeTodo(item);
  }

  /**
   * Metodo para editar un elemento de la lista
   * @param item 
   */
  showEditTodo(todo:TodoModel){
    let modal = this.modalController.create(AddTaskModalPage, {todo});//lanza la pagina de Add new task, para editarla
    modal.present();
    modal.onDidDismiss(data => {
      this.todoServices.showEditTodo(todo, data);
    })
    
  }
}
